package com.example.demo.api;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ErrorResponse {
    private HttpStatus httpStatus;
    private String code;
    private String message;
}
