package com.example.demo.api;

import java.math.BigDecimal;

import com.example.demo.exception.ApiException;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class PostTransactionRequest {

  private BigDecimal amount;

  public void setAmount(BigDecimal amount) {
    if (amount.doubleValue()<0){
      throw new ApiException(HttpStatus.BAD_REQUEST,
              "TRANSACTION_AMOUNT_LT_ZERO",
              "Transaction amount is les than 0.");
    }
    this.amount = amount;
  }
}
