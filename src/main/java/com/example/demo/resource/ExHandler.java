package com.example.demo.resource;

import com.example.demo.api.ErrorResponse;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.InternalTransactionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExHandler {


  @ExceptionHandler(InternalTransactionException.class)
  void doNothing() {

  }

  @ExceptionHandler(ApiException.class)
  public void handleApiEx(ApiException ex){
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setHttpStatus(ex.getHttpStatus());
    errorResponse.setCode(ex.getCode());
    errorResponse.setMessage(ex.getMessage());

  }
}
