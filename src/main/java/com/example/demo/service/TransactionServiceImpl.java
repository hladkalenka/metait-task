package com.example.demo.service;


import com.example.demo.api.PostTransactionRequest;
import com.example.demo.domain.BankAccount;
import com.example.demo.exception.InternalTransactionException;
import com.example.demo.mapper.BankAccountMapper;
import com.example.demo.repository.BankAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Component
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService{
    private final BankAccountRepository bankAccountRepository;
    private final BankAccountMapper bankAccountMapper;

    @Transactional
    @Override
    public void createTransaction(Long account, PostTransactionRequest request) {
        Optional<BankAccount> bankAccount = bankAccountRepository.findById(account);
        BigDecimal balance = bankAccount.get().getBalance();

        //transakce

        //balance after trensaction
        if (balance.doubleValue() < 200){
            throw new InternalTransactionException();
        }

        if (request.getAmount().doubleValue()<0){
            throw new IllegalArgumentException("The amount of the transaction cannot be negative");

        }
    }
}
